import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: "app-geodata",
  templateUrl: "./geodata.component.html",
  styleUrls: ["./geodata.component.css"]
})
export class GeodataComponent {
  public data;
  jsons = [
    {
      country: "US",
      name: "Massachusetts",
      key: "3864"
    },

    {
      country: "US",
      primary_city_id: 2464828,
      name: "02111",
      key: "US:02111",
      region_id: 3864
    },
    {
      country: "US",
      name: "Massachusetts",
      key: "3864"
    },
    {
      name: "Boston",
      country: "US",
      region: "Massachusetts",
      distance_unit: "mile",
      key: "2464828",
      region_id: "3864"
    },
    {
      name: "Cambridge",
      country: "US",
      region: "Massachusetts",
      distance_unit: "mile",
      key: "2464909",
      region_id: "3864"
    },
    {
      name: "Somerville",
      country: "US",
      region: "Massachusetts",
      distance_unit: "mile",
      key: "2466348",
      region_id: "3864"
    },
    {
      name: "Boston",
      country: "US",
      region: "Massachusetts",
      distance_unit: "mile",
      key: "2464828",
      region_id: "3864"
    },
    {
      name: "Cambridge",
      country: "US",
      region: "Massachusetts",
      distance_unit: "mile",
      key: "2464909",
      region_id: "3864"
    },
    {
      name: "Somerville",
      country: "US",
      region: "Massachusetts",
      distance_unit: "mile",
      key: "2466348",
      region_id: "3864"
    }
  ];

  keys = ["region", "cities", "country", "key"];
  values;

  constructor(http: HttpClient, @Inject("BASE_URL") baseUrl: string) {
    /* Uncomment to process API request */

 http.get<GeoLocation[]>("baseUrl"+ 'api/SampleData/GeoData').subscribe(result => {

      this.processData(result)
   }, error => console.error(error));
    this.processData(this.jsons);
  }

  processData(data) {
    //this.keys = Object.keys(data);
        this.values = data;
  }
  removeRow(item) {
    var index = this.values.indexOf(item);
    this.values.splice(index, 1);
  }


}


interface GeoLocation {
  locationName: string;
  locationKey: number;
  locationType: string;
}
