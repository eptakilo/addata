﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace adTarg.Controllers
{
    public static class DataGeo
    {
        public static string GetData()
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), @"data.json");
            var data = File.ReadAllText(path);

            DataType.RootObject dataList = JsonConvert.DeserializeObject<DataType.RootObject>(data);

            //Get list of all current targetting objects
            List<DataType.Targeting> TargetingList = dataList.data.Select(t => t.targeting).ToList();
            var targetting = JsonConvert.SerializeObject(TargetingList);
            //Get Geolocations

            //Added JsonIgnore Attribute to skip location types, since home repeats 

            List<DataType.GeoLocations> geoLocations = TargetingList.Select(i => i.geo_locations).ToList();
       
            //Convert to Json ignoring nulls
            var jsonLocations = JsonConvert.SerializeObject(geoLocations, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            //---------------------------------------------
            var geoArray = JArray.Parse(jsonLocations);
            dynamic zips;

            JArray wantedValues = new JArray();

            //Big O is a big no no here but I just want it to work.,
            // BIG O OF : N* N2 * N2 * N2  = O(N7) 
            foreach (var item in geoArray)
            {
              

                foreach (var obj in item)
                {
                    foreach(var stuff in obj)
                    {
                        foreach(var array in stuff)
                        {

                            wantedValues.Add(array);
                           // var test = JsonConvert.SerializeObject(array);
                        }

                       
                    }
                }
            }

            var top5 = wantedValues
             .GroupBy(value => value)
             .OrderByDescending(group => group.Count())
             .Select(group => group.Key)
             .Take(5);

            var transformedJSON = JsonConvert.SerializeObject(wantedValues, Formatting.Indented);
            return JsonConvert.SerializeObject(top5, Formatting.Indented);


            
        }




    }
 
}
