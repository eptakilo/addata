﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace adTarg.Controllers
{
    public class DataType
    {
        public class Interest
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class WorkEmployer
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class LifeEvent
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class Behavior
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class CustomAudience
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class FlexibleSpec
        {
            public List<Interest> interests { get; set; }
            public List<WorkEmployer> work_employers { get; set; }
            public List<LifeEvent> life_events { get; set; }
            public List<Behavior> behaviors { get; set; }
            public List<CustomAudience> custom_audiences { get; set; }
        }

        public class Region
        {
            public string country { get; set; }
            public string name { get; set; }
            public string key { get; set; }
        }

        public class Zip
        {
            public string country { get; set; }
            public int primary_city_id { get; set; }
            public string name { get; set; }
            public string key { get; set; }
            public int region_id { get; set; }
        }

        public class City
        {
            public string name { get; set; }
            public string country { get; set; }
            public string region { get; set; }
            public string distance_unit { get; set; }
            public string key { get; set; }
            public string region_id { get; set; }
            public int? radius { get; set; }
        }

        public class GeoMarket
        {
            public string country { get; set; }
            public string name { get; set; }
            public string key { get; set; }
        }

        public class GeoLocations
        {
            public List<Region> regions { get; set; }

            [JsonIgnore]
            public List<string> location_types { get; set; }
            public List<Zip> zips { get; set; }
            public List<City> cities { get; set; }
            public List<string> countries { get; set; }
            public List<GeoMarket> geo_markets { get; set; }
        }

        public class Interest2
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class Exclusions
        {
            public List<Interest2> interests { get; set; }
        }

        public class CustomAudience2
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class City2
        {
            public string name { get; set; }
            public string country { get; set; }
            public string region { get; set; }
            public string distance_unit { get; set; }
            public int radius { get; set; }
            public string key { get; set; }
            public string region_id { get; set; }
        }

        public class Region2
        {
            public string country { get; set; }
            public string name { get; set; }
            public string key { get; set; }
        }

        public class ExcludedGeoLocations
        {
            public List<City2> cities { get; set; }
            public List<string> location_types { get; set; }
            public List<Region2> regions { get; set; }
        }

        public class Interest3
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class ExcludedCustomAudience
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class Targeting
        {
            public List<FlexibleSpec> flexible_spec { get; set; }
            public List<string> publisher_platforms { get; set; }
            public GeoLocations geo_locations { get; set; }
            public List<string> audience_network_positions { get; set; }
            public List<string> instagram_positions { get; set; }
            public int age_min { get; set; }
            public List<string> device_platforms { get; set; }
            public List<string> facebook_positions { get; set; }
            public int age_max { get; set; }
            public List<string> messenger_positions { get; set; }
            public List<int?> genders { get; set; }
            public Exclusions exclusions { get; set; }
            public List<CustomAudience2> custom_audiences { get; set; }
            public ExcludedGeoLocations excluded_geo_locations { get; set; }
            public List<Interest3> interests { get; set; }
            public string targeting_optimization { get; set; }
            public List<int?> locales { get; set; }
            public List<ExcludedCustomAudience> excluded_custom_audiences { get; set; }
            public List<string> user_os { get; set; }
        }

        public class Datum
        {
            public Targeting targeting { get; set; }
            public string id { get; set; }
        }

        public class RootObject
        {
            public List<Datum> data { get; set; }
        }
    }

    
}
