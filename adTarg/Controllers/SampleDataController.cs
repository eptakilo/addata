using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace adTarg.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        
        [HttpGet("[action]")]
        public ActionResult GeoData()
        {
            var data = DataGeo.GetData();
            
            return Ok(data);
        }

       
    }
}
